
import os
import json

TEMPLATE = """
<!DOCTYPE html>
<meta charset="UTF-8"> 
<html>

<head>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.5.3/css/bulma.min.css" integrity="sha256-spCEAaZMKebC3rE/ZTt8jITn65b0Zan45WXblWjyDyQ=" crossorigin="anonymous" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <script
    src="https://code.jquery.com/jquery-3.2.1.min.js"
    integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
    crossorigin="anonymous"></script>
</head>

<body onload="initialize()">

<nav class="navbar" role="navigation" aria-label="main navigation">
    <div class="navbar-brand">
        <a class="navbar-item">
            Japanese Flash-cards
        </a>
    </div>
</nav>

<div class="columns">
    <div class="column is-one-quarter">
        <div class="section">
            <div class="container">
                <h2 class="title">Select Decks</h2>
                <div class="content">
                    <p>
                        Total cards selected: <span id="card_count">0</span>
                    </p>
                </div>
                <div class="select is-multiple">
                    <select multiple size="8" id="decks" onchange="filter_cards()">
                    </select>
                </div>
            </div>
        </div>
    </div>
    <div class="column">
        <div class="section">
            <div class="box">
                <div class="level">
                    <div class="left-item">
                        <strong>Context:</strong> <span id="context">Place</span>
                    </div>
                </div>
                <div class="level">
                    <div class="level-item">
                        <p style="font-weight:500;font-size:5rem" id="japanese">
                            <ruby>日<rt>に</rt>本<rt>ほん</rt>人<rt>じん</rt></ruby>
                        </h1>
                    </div>
                </div>
                <div class="level">
                    <div class="level-item">
                        <p style="font-size:2rem" id="romaji">
                            ni hon jin
                        </p>
                    </div>
                </div>
                <div class="level">
                    <div class="level-item">
                        <p style="font-size:2rem" id="english">
                            Japanese (person)
                        </p>
                    </div>
                </div>
                <div class="level">
                    <div class="left-item">
                        <button class="button is-success" onclick="toggle_furigana()" id="furigana">Show Furigana</button>
                    </div>
                    <div class="right-item" id="submit">
                        <button class="button is-primary" onclick="reveal()" id="reveal">Reveal</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    const decks = """

TEMPLATE2 = """

    var selected_decks = [];

    const cards = """

TEMPLATE3 = """

    var available_cards = [];
    var current_card = 0;
    var show_furigana = false;
    var show_answer = false;

    function initialize() {        
        reset()

        decks.forEach(function(element) {
            let s = $("<option>").prop("value", element).html(element)
            $("#decks").append(s)
        }, this);

        true
    }

    function do_intersect(a, b)
    {
    var ai=0, bi=0;
    var result = [];

    for(ai = 0; ai < a.length; ai++) {
        for (bi = 0; bi < b.length; bi++) {
            if (a[ai] == b[bi]) {
                return true
            }
        }
    }

    return false
    }

    function filter_cards() {
        selected_decks = []
        $("#decks").find(":selected").each( function (i) { 
            selected_decks.push($(this).prop("value"))
        })

        available_cards = []
        cards.forEach(function(element) {
            if (do_intersect(element['group'], selected_decks)) {
                available_cards.push(element)
            }
        }, this)

        $("#card_count").html(available_cards.length)
        current_card = 0
        shuffle(available_cards)
        select_card();
    }

    function reset() {
        $("rt").css('visibility', 'hidden')
        $("#english").css('visibility', 'hidden')
        $("#romaji").css('visibility', 'hidden')

        $("#furigana").removeClass("is-danger").addClass("is-success").html("Show Furigana");
        $("#reveal").removeClass("is-warning").addClass("is-primary").html("Reveal")
        
        show_furigana = false;
        show_answer = false;
    }

    function set_furigana() {
        if (show_furigana) {
            $("#furigana").removeClass("is-success").addClass("is-danger").html("Hide Furigana");
            $("rt").css('visibility', 'visible')
        } else {
            $("#furigana").removeClass("is-danger").addClass("is-success").html("Show Furigana");
            $("rt").css('visibility', 'hidden')
        }
    }

    function reveal() {
        if (!show_answer) {
            show_answer = true
            show_furigana = true
            $("#english").css('visibility', 'visible')
            $("#romaji").css('visibility', 'visible')
            set_furigana()

            $("#reveal").removeClass("is-primary").addClass("is-warning").html("Next")
        } else {
            reset()
            select_card();
        }
    }

    function toggle_furigana() {
        show_furigana = !show_furigana
        set_furigana()
        true
    }

    function shuffle(a) {
        var j, x, i;
        for (i = a.length; i; i--) {
            j = Math.floor(Math.random() * i);
            x = a[i - 1];
            a[i - 1] = a[j];
            a[j] = x;
        }
    }

    function select_card() {
        if (available_cards.length == 0) {
            return;
        }

        current_card += 1
        if (current_card >= available_cards.length) {
            shuffle(available_cards)
            current_card = 0
        }

        var card = available_cards[current_card];

        var card_text = $("<ruby>").html(card["japanese"])
        //var card_text = card["japanese"]

        $("#context").html(card["context"])
        $("#japanese").html(card_text)
        $("#romaji").html(card["romaji"])
        $("#english").html(card["english"])
        set_furigana();
    }
</script>
</body>
</html>
"""

cards = []

for root, dirs, files in os.walk("./cards"):
    for f in files:
        with open(os.path.join(root,f), 'r', encoding='UTF-8') as data:
            print(f)
            cards += json.load(data)

decks = set()

for card in cards:
    decks |= set(card['group'])

decks = list(decks)
decks = sorted(decks)

with open('index.html', 'w', encoding="UTF-8") as out:
    t = TEMPLATE + json.dumps(decks) + TEMPLATE2 + json.dumps(cards) + TEMPLATE3
    out.write(t)


